import argparse
import functools
from collections import defaultdict

import numpy as np
import pandas as pd
import xgboost as xgb
import jellyfish as jf

from nltk.corpus import stopwords
from collections import Counter
from sklearn.metrics import log_loss
from sklearn.cross_validation import train_test_split

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer

from xgboost import XGBClassifier
 

def word_match_share(row, stops=None):
    q1words = {}
    q2words = {}
    for word in row['question1']:
        if word not in stops:
            q1words[word] = 1
    for word in row['question2']:
        if word not in stops:
            q2words[word] = 1
    if len(q1words) == 0 or len(q2words) == 0:
        # The computer-generated chaff includes a few questions that are nothing but stopwords
        return 0
    shared_words_in_q1 = [w for w in q1words.keys() if w in q2words]
    shared_words_in_q2 = [w for w in q2words.keys() if w in q1words]
    R = (len(shared_words_in_q1) + len(shared_words_in_q2))/(len(q1words) + len(q2words))
    return R

def levenshtein_distance(row):
    s1 = row['question1']
    s2 = row['question2']
    
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]

def damerau_levenshtein_distance(row):
    s1 = row['question1']
    s2 = row['question2']
    
    return  jf.damerau_levenshtein_distance(s1, s2)



def qcdiff(row):
    s1 = row['question1']
    s2 = row['question2']
    
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
        
    l1 = len(''.join(row['question1'])) 
    l2 = len(''.join(row['question2']))
    qcdiff = 1 - distances[-1]/(l1+l2)
    
    return qcdiff

def jaro_distance(row):
    s1 = row['question1']
    s2 = row['question2']
    
    return jf.jaro_distance(s1, s2)

def jaro_winkler(row):
    s1 = row['question1']
    s2 = row['question2']
    
    return jf.jaro_winkler(s1, s2)

def cos(row):
    s1 = row['question1']
    s2 = row['question2']
    
    train_set = list()
    train_set.append(s1)
    train_set.append(s2)

    tfidf_vectorizer = TfidfVectorizer()
    tfidf_matrix_train = tfidf_vectorizer.fit_transform(train_set)  #finds the tfidf score with normalization
    # return lambda x: strip_accents(x.lower())AttributeError: ("'list' object has no attribute 'lower'", u'occurred at index 0')

    cs = cosine_similarity(tfidf_matrix_train[0:1], tfidf_matrix_train)  #here the first element of tfidf_matr
    #ix_train is matched with other three elements
    return float( cs.item(1)  )



def jaccard(row):
    wic = set(row['question1']).intersection(set(row['question2']))
    uw = set(row['question1']).union(row['question2'])
    if len(uw) == 0:
        uw = [1]
    return (len(wic) / len(uw))

def common_words(row):
    return len(set(row['question1']).intersection(set(row['question2'])))

def total_unique_words(row):
    return len(set(row['question1']).union(row['question2']))

def total_unq_words_stop(row, stops):
    return len([x for x in set(row['question1']).union(row['question2']) if x not in stops])

def wc_diff(row):
    return abs(len(row['question1']) - len(row['question2']))

def wc_ratio(row):
    l1 = len(row['question1'])*1.0 
    l2 = len(row['question2'])
    if l2 == 0:
        return np.nan
    if l1 / l2:
        return l2 / l1
    else:
        return l1 / l2

def wc_diff_unique(row):
    return abs(len(set(row['question1'])) - len(set(row['question2'])))

def wc_ratio_unique(row):
    l1 = len(set(row['question1'])) * 1.0
    l2 = len(set(row['question2']))
    if l2 == 0:
        return np.nan
    if l1 / l2:
        return l2 / l1
    else:
        return l1 / l2

def wc_diff_unique_stop(row, stops=None):
    return abs(len([x for x in set(row['question1']) if x not in stops]) - len([x for x in set(row['question2']) if x not in stops]))

def wc_ratio_unique_stop(row, stops=None):
    l1 = len([x for x in set(row['question1']) if x not in stops])*1.0 
    l2 = len([x for x in set(row['question2']) if x not in stops])
    if l2 == 0:
        return np.nan
    if l1 / l2:
        return l2 / l1
    else:
        return l1 / l2

def same_start_word(row):
    if not row['question1'] or not row['question2']:
        return np.nan
    return int(row['question1'][0] == row['question2'][0])

 
 
def char_diff(row):
    return abs(len(''.join(row['question1'])) - len(''.join(row['question2'])))

def char_ratio(row):
    l1 = len(''.join(row['question1'])) 
    l2 = len(''.join(row['question2']))
    if l2 == 0:
        return np.nan
    if l1 / l2:
        return l2 / l1
    else:
        return l1 / l2

def char_diff_unique_stop(row, stops=None):
    return abs(len(''.join([x for x in set(row['question1']) if x not in stops])) - len(''.join([x for x in set(row['question2']) if x not in stops])))


def get_weight(count, eps=10000, min_count=2):
    if count < min_count:
        return 0
    else:
        return 1 / (count + eps)
    
def tfidf_word_match_share_stops(row, stops=None, weights=None):
    q1words = {}
    q2words = {}
    for word in row['question1']:
        if word not in stops:
            q1words[word] = 1
    for word in row['question2']:
        if word not in stops:
            q2words[word] = 1
    if len(q1words) == 0 or len(q2words) == 0:
        # The computer-generated chaff includes a few questions that are nothing but stopwords
        return 0
    
    shared_weights = [weights.get(w, 0) for w in q1words.keys() if w in q2words] + [weights.get(w, 0) for w in q2words.keys() if w in q1words]
    total_weights = [weights.get(w, 0) for w in q1words] + [weights.get(w, 0) for w in q2words]
    
    R = np.sum(shared_weights) / np.sum(total_weights)
    return R

def tfidf_word_match_share(row, weights=None):
    q1words = {}
    q2words = {}
    for word in row['question1']:
        q1words[word] = 1
    for word in row['question2']:
        q2words[word] = 1
    if len(q1words) == 0 or len(q2words) == 0:
        # The computer-generated chaff includes a few questions that are nothing but stopwords
        return 0
    
    shared_weights = [weights.get(w, 0) for w in q1words.keys() if w in q2words] + [weights.get(w, 0) for w in q2words.keys() if w in q1words]
    total_weights = [weights.get(w, 0) for w in q1words] + [weights.get(w, 0) for w in q2words]
    # if np.sum(total_weights)==0
    R = np.sum(shared_weights) / np.sum(total_weights)
    return R


def build_features(data, stops, weights):
    X = pd.DataFrame()
    f = functools.partial(word_match_share, stops=stops)
    X['word_match'] = data.apply(f, axis=1, raw=True) #1

    f = functools.partial(tfidf_word_match_share, weights=weights)
    X['tfidf_wm'] = data.apply(f, axis=1, raw=True) #2

    f = functools.partial(tfidf_word_match_share_stops, stops=stops, weights=weights)
    X['tfidf_wm_stops'] = data.apply(f, axis=1, raw=True) #3

    X['jaccard'] = data.apply(jaccard, axis=1, raw=True) #4
    X['wc_diff'] = data.apply(wc_diff, axis=1, raw=True) #5
    X['wc_ratio'] = data.apply(wc_ratio, axis=1, raw=True) #6
    X['wc_diff_unique'] = data.apply(wc_diff_unique, axis=1, raw=True) #7
    X['wc_ratio_unique'] = data.apply(wc_ratio_unique, axis=1, raw=True) #8

    f = functools.partial(wc_diff_unique_stop, stops=stops)    
    X['wc_diff_unq_stop'] = data.apply(f, axis=1, raw=True) #9
    f = functools.partial(wc_ratio_unique_stop, stops=stops)    
    X['wc_ratio_unique_stop'] = data.apply(f, axis=1, raw=True) #10

    X['same_start'] = data.apply(same_start_word, axis=1, raw=True) #11
    X['char_diff'] = data.apply(char_diff, axis=1, raw=True) #12

    f = functools.partial(char_diff_unique_stop, stops=stops) 
    X['char_diff_unq_stop'] = data.apply(f, axis=1, raw=True) #13

#     X['common_words'] = data.apply(common_words, axis=1, raw=True)  #14
    X['total_unique_words'] = data.apply(total_unique_words, axis=1, raw=True)  #15

    f = functools.partial(total_unq_words_stop, stops=stops)
    X['total_unq_words_stop'] = data.apply(f, axis=1, raw=True)  #16
    
    X['char_ratio'] = data.apply(char_ratio, axis=1, raw=True) #17    
    
    X['levenshtein_distance'] = data.apply(levenshtein_distance, axis=1, raw=True) #18
    X['qcdiff'] = data.apply(qcdiff, axis=1, raw=True) #19 
    X['jaro_distance'] = data.apply(jaro_distance, axis=1, raw=True) #20
    X['damerau_levenshtein_distance'] = data.apply(damerau_levenshtein_distance, axis=1, raw=True) #21
    X['jaro_winkler'] = data.apply(jaro_winkler, axis=1, raw=True) #22
    X['cos'] = data.apply(cos, axis=1, raw=True) #23

    return X


def main():
    parser = argparse.ArgumentParser(description='XGB with Handcrafted Features')
    parser.add_argument('--save', type=str, default='XGB_leaky',
                        help='save_file_names')
    args = parser.parse_args()

    df_train = pd.read_csv('/home/yoka/Desktop/R&D/Kaggle/quora/train_features.csv', encoding="ISO-8859-1")
    
    #################### drop_duplicates
#    df_train=df_train.drop_duplicates(['question1'])
#    df_train=df_train.drop_duplicates(['question2'])
#    
#    print ("df_train dim 1 : ")
#    print(  (df_train).shape ) 
#    
#    X_train_ab = df_train.iloc[:, 2:-1]
#    
#    print ("X_train_ab dim 2 : ")
#    print(  (X_train_ab).shape ) 
#    
#    X_train_ab = X_train_ab.drop('euclidean_distance', axis=1)
#    X_train_ab = X_train_ab.drop('jaccard_distance', axis=1)
#
#    print ("X_train_ab dim 3 : ")
#    print(  (X_train_ab).shape ) 
#
#    df_train = pd.read_csv('/home/yoka/Desktop/R&D/Kaggle/quora/train.csv')
#    df_train = df_train.fillna(' ') 
#    
#    #################### drop_duplicates
#    
#    df_train=df_train.drop_duplicates(['question1'])
#    df_train=df_train.drop_duplicates(['question2'])
  

    
    df_test = pd.read_csv('/home/yoka/Desktop/R&D/Kaggle/quora/test.csv')
    ques = pd.concat([df_train[['question1', 'question2']], \
        df_test[['question1', 'question2']]], axis=0).reset_index(drop='index')
    q_dict = defaultdict(set)
    for i in range(ques.shape[0]):
            q_dict[ques.question1[i]].add(ques.question2[i])
            q_dict[ques.question2[i]].add(ques.question1[i])

    def q1_freq(row):
        return(len(q_dict[row['question1']]))
        
    def q2_freq(row):
        return(len(q_dict[row['question2']]))
        
    def q1_q2_intersect(row):
        return(len(set(q_dict[row['question1']]).intersection(set(q_dict[row['question2']]))))

    df_train['q1_q2_intersect'] = df_train.apply(q1_q2_intersect, axis=1, raw=True)
    df_train['q1_freq'] = df_train.apply(q1_freq, axis=1, raw=True)
    df_train['q2_freq'] = df_train.apply(q2_freq, axis=1, raw=True)

    df_test['q1_q2_intersect'] = df_test.apply(q1_q2_intersect, axis=1, raw=True)
    df_test['q1_freq'] = df_test.apply(q1_freq, axis=1, raw=True)
    df_test['q2_freq'] = df_test.apply(q2_freq, axis=1, raw=True)

    test_leaky = df_test.loc[:, ['q1_q2_intersect','q1_freq','q2_freq']]
    del df_test

    train_leaky = df_train.loc[:, ['q1_q2_intersect','q1_freq','q2_freq']]

    # explore
    stops = set(stopwords.words("english"))

    df_train['question1'] = df_train['question1'].map(lambda x: str(x).lower().split())
    df_train['question2'] = df_train['question2'].map(lambda x: str(x).lower().split())

    train_qs = pd.Series(df_train['question1'].tolist() + df_train['question2'].tolist())

    words = [x for y in train_qs for x in y]
    counts = Counter(words)
    weights = {word: get_weight(count) for word, count in counts.items()}

    print('Building Features')
    print ("df_train dim 4 : ")
    print(  (df_train).shape )
    X_train = build_features(df_train, stops, weights)
    X_train = pd.concat((X_train, X_train_ab, train_leaky), axis=1)
    y_train = df_train['is_duplicate'].values
    
    print( "(X_train).shape[0]" ) 
    print( (X_train).shape[0] ) 
    
    print( "(y_train).shape[0]" ) 
    print( (y_train).shape[0] ) 
     
#    print( "type( str X_valid" )    


    X_train = X_train.drop(X_train.index[len(X_train)-1])

    X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.1, random_state=4242)

    #UPDownSampling
    pos_train = X_train[y_train == 1]
    neg_train = X_train[y_train == 0]
    X_train = pd.concat((neg_train, pos_train.iloc[:int(0.8*len(pos_train))], neg_train))
    y_train = np.array([0] * neg_train.shape[0] + [1] * pos_train.iloc[:int(0.8*len(pos_train))].shape[0] + [0] * neg_train.shape[0])
    print(np.mean(y_train))
    del pos_train, neg_train

    pos_valid = X_valid[y_valid == 1]
    neg_valid = X_valid[y_valid == 0]
    X_valid = pd.concat((neg_valid, pos_valid.iloc[:int(0.8 * len(pos_valid))], neg_valid))
    y_valid = np.array([0] * neg_valid.shape[0] + [1] * pos_valid.iloc[:int(0.8 * len(pos_valid))].shape[0] + [0] * neg_valid.shape[0])
    print(np.mean(y_valid))
    del pos_valid, neg_valid


    params = {}
    params['objective'] = 'binary:logistic'
    params['eval_metric'] = 'logloss'
    params['eta'] = 0.02
    params['max_depth'] = 7
    params['subsample'] = 0.6
    params['base_score'] = 0.2
    # params['scale_pos_weight'] = 0.2

    d_train = xgb.DMatrix(X_train, label=y_train)
    d_valid = xgb.DMatrix(X_valid, label=y_valid)

    watchlist = [(d_train, 'train'), (d_valid, 'valid')]

#     bst = xgb.train(params, d_train, 2500, watchlist, early_stopping_rounds=50, verbose_eval=50)
   # print ("df_train dim 5 : ")
   # print(  (d_train).shape )  
    
    bst = xgb.train(params, d_train, 2500, watchlist, early_stopping_rounds=50, verbose_eval=50)
    #pd.DataFrame(bst.get_fscore().items(), columns=['feature','importance']).sort_values('importance', ascending=False)
    #xgb.plot_importance(bst)
    
    print(log_loss(y_valid, bst.predict(d_valid)))
    bst.save_model(args.save + '.mdl')


    print('Building Test Features')
    df_test = pd.read_csv('/home/yoka/Desktop/R&D/Kaggle/quora/test_features.csv', encoding="ISO-8859-1")
    x_test_ab = df_test.iloc[:, 2:-1]
    x_test_ab = x_test_ab.drop('euclidean_distance', axis=1)
    x_test_ab = x_test_ab.drop('jaccard_distance', axis=1)
    
    df_test = pd.read_csv('/home/yoka/Desktop/R&D/Kaggle/quora/test.csv')
    df_test = df_test.fillna(' ')

    df_test['question1'] = df_test['question1'].map(lambda x: str(x).lower().split())
    df_test['question2'] = df_test['question2'].map(lambda x: str(x).lower().split())
    
    x_test = build_features(df_test, stops, weights)
    x_test = pd.concat((x_test, x_test_ab, test_leaky), axis=1)
    d_test = xgb.DMatrix(x_test) # MemoryError Exception AttributeError: "'DMatrix' object has no attribute 'handle'" in <bound method DMatrix.__del__ of <xgboost.core.DMatrix object at 0x7f7baefd4090>> ignored
    p_test = bst.predict(d_test)
    sub = pd.DataFrame()
    sub['test_id'] = df_test['test_id']
    sub['is_duplicate'] = p_test
    sub.to_csv('/home/yoka/Desktop/R&D/Kaggle/quora/' + args.save + '.csv')

if __name__ == '__main__':
    main()
    
    ########### results #############
    
    # basic - base features
    #   bst = xgb.train(params, d_train, 250, watchlist, early_stopping_rounds=3, verbose_eval=50)
    # valid-logloss:0.28018
    
    # basic - base features + lev'
    #  valid-logloss:0.279804

    
    # basic - base features + lev' + "Qcdiff"
    #  valid-logloss:0.279786
    
    # basic - base features + lev' + "Qcdiff" + jaro_distance  == NBS
    #  valid-logloss:0.279593

    # NBS + damerau_levenshtein_distance 
    #  valid-logloss:0.279585

    # NBS + damerau_levenshtein_distance + jaro_winkler
    #  valid-logloss:0.279033
    
    # NBS + damerau_levenshtein_distance + jaro_winkler +  
    #   0.279195
    
    # NBS + damerau_levenshtein_distance + jaro_winkler + no duplicat 
    #  valid-logloss:0.357407
    
    
    
    
    #### all
    
   # NBS + damerau_levenshtein_distance + jaro_winkler
   # valid-logloss:0.189483